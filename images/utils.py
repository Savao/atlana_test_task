from io import BytesIO
from PIL import Image

from django.core.files.uploadedfile import InMemoryUploadedFile


def resize_and_get_square(img: InMemoryUploadedFile, max_size: int = 0, name: str = ''):
    """
    Resize image keeping ratio and using white background.
    """
    if not name:
        name = img.name
    image = Image.open(img)
    image_format = image.format
    if not max_size:
        if image.width == image.height:
            img.name = name
            return img
        max_size = max(image.width, image.height)
    new_size = {'width': max_size, 'height': max_size}
    aspect_ratio = image.width / image.height
    if image.width < image.height:
        new_size['width'] = int(max_size * aspect_ratio)
    if image.width > image.height:
        new_size['height'] = int(max_size / aspect_ratio)
    image_resize = image.resize((new_size['width'], new_size['height']), Image.LANCZOS)
    image.close()
    if new_size['height'] != new_size['width']:
        background = Image.new('RGBA', (max_size, max_size), (255, 255, 255, 255))
        offset = (round((max_size - new_size['width']) / 2), round((max_size - new_size['height']) / 2))
        background.paste(image_resize, offset)
        image_resize = background.convert('RGB')
    img_io = BytesIO()
    image_resize.save(img_io, format=image_format)
    return InMemoryUploadedFile(img_io, None, name, img.content_type, img_io.tell, None)
