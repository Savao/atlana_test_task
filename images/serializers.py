from rest_framework import serializers


class ImageSerializer(serializers.Serializer):
    """
    Serializer for image upload
    """
    CHOICES = ['Original', 'Square of original size', 'Small', 'All three']
    image = serializers.ImageField()
    output = serializers.ChoiceField(choices=CHOICES)
