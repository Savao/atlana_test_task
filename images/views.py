from os import path

from boto3 import Session
from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.decorators import parser_classes, api_view


from .serializers import ImageSerializer
from .utils import resize_and_get_square


@swagger_auto_schema(
    method='post',
    request_body=ImageSerializer,
    responses={200: 'dict: {"success":"true"}',
               400: ''}
)
@api_view(['POST'])
@parser_classes((MultiPartParser,))
def image(request):
    serializer = ImageSerializer(data=request.data)
    if serializer.is_valid():
        session = Session(
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
        )
        images = list()
        s3 = session.client('s3')
        img = request.FILES['image']
        name, ext = path.splitext(img.name)
        output = serializer.data.get('output')
        choices = ImageSerializer.CHOICES
        if output in [choices[0], choices[3]]:
            result_name = f'{name}_original{ext}'
            img.name = result_name
            images.append(img)
        if output in [choices[1], choices[3]]:
            result_name = f'{name}_squared{ext}'
            img.file.seek(0)
            resized_img = resize_and_get_square(img, name=result_name)
            images.append(resized_img)
        if output in [choices[2], choices[3]]:
            result_name = f'{name}__small_squared{ext}'
            img.file.seek(0)
            resized_img = resize_and_get_square(img, max_size=256, name=result_name)
            images.append(resized_img)
        for image in images:
            image.file.seek(0)
            s3.upload_fileobj(image.file, settings.AWS_STORAGE_BUCKET_NAME, image.name)
        return Response({'success': True}, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
